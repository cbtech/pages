var result = 0;

var score = 0;
var good = 0;
var level = 0;
var wrong = 0;
var errors = [];
var started = false;

var add = 0;
var sub = 0;
var mul = 0;
var sign;
var calc =[0,0,0,150];

var swap_difficulty = 0;
var multiply_difficulty = 10;
var timer = 145; // 5 Sec/calc = 50
var min = Math.round(timer / 60);
var sec = Math.round(timer % 60);
var nb_calc = 0;
var average = 0;

function init() {
	errors.length = 0;
	document.getElementById("timer").style.visibility = "visible";
	document.getElementById("cert").style.visibility = "hidden";
	document.getElementById("startBTN").style.visibility = "hidden";
	show_calc(); 
	started = false; 
	clearBox('c_errors');
	update_score(score);
  //check_selection();
}
/*
function toggle_sel(selection) {
// Toggle selection in menu
		if(selection == "add") {
			if(add == 0) {
			  document.getElementById("BTN_Plus").src = 'Res/BTN_Plus[HOVER].png'; 
			  add = 1;
			} else {
			    document.getElementById("BTN_Plus").src = 'Res/BTN_Plus.png';
			    add = 0;
			}
		}

		if(selection == "sub") {
			if(sub == 0) {
			  document.getElementById("BTN_Sub").src = 'Res/BTN_Minus[HOVER].png'; 
			  sub = 1;
			} else {
			    document.getElementById("BTN_Sub").src = 'Res/BTN_Minus.png';
			    sub = 0;
			}
		}

		if(selection == "mul") {
			if(mul == 0) {
			  document.getElementById("BTN_Mul").src = 'Res/BTN_Mul[HOVER].png';
			  mul = 1;
			} else {
			    document.getElementById("BTN_Mul").src = 'Res/BTN_Mul.png';
			    mul = 0;
			}
		}
}

function check_selection() {
// Verify if there at least one selected item before starting
	console.log("add: Sub:  sub: ",add,sub,mul); 
	if (add == 0 && sub ==0 && mul == 0) {
		document.getElementById("math_page").setAttribute('href','menu.html');
		alert("Select at least one item");
	}
	if(add == 1) 
		document.getElementById("BTN_Plus").src = 'Res/BTN_Plus[HOVER].png';
	if(sub == 1)
		document.getElementById("BTN_Sub").src = 'Res/BTN_Minus[HOVER].png';
	if(mul == 1)
		document.getElementById("BTN_Mul").src = 'Res/BTN_Mul[HOVER].png';
}
*/
function update_score(new_score) {
	score += new_score;
	if(score % 50 === 0) { level++; calc[3] = level * 10;}

	document.getElementById("score").innerHTML = "Score:   " + score;
	document.getElementById("level").innerHTML = "Level:   " + level;
	document.getElementById("wrong").innerHTML = "Wrong:   " + wrong;
	document.getElementById("good").innerHTML = "Good:   " + good;
}

function clearBox(elementID)
{
   // document.getElementById(elementID).innerHTML = "";
}


function show_certificate() {
	average = (good - wrong) / 2;
	document.getElementById("cert").style.visibility = "visible";
  document.getElementById("calc").innerHTML = "Amazing !!!";
  document.getElemetnById("res_input").innerHTML = " "; 
	document.getElementById("startBTN").style.visibility = "visible";
	document.getElementById("timer").style.visibility = "hidden";
	document.getElementById("c_score").innerHTML = score;
	document.getElementById("c_good").innerHTML = good;
	document.getElementById("c_bad").innerHTML = wrong;
	document.getElementById("c_note").innerHTML = average + " / 20";
	show_errors();
}

var show_errors = (function() {
	return function() {
		if(!started) {
			started = true;
			var errors_div = document.getElementById('c_errors');

			for(var i =0; i < errors.length;i++) {
				errors_div.innerHTML += errors[i] + "<br />";
				
			}
		}
	};
}) ();

function update(state) {
	if(state == "good") {
		good += 1;
	} else {
		wrong +=1;
	}
	document.getElementById("wrong").innerHTML = "Wrong:   " + wrong;
	document.getElementById("good").innerHTML = "Good:   " + good;
}

function dec_timer() {
	if(timer == 0) {
		show_certificate();
		return;
	}
	if(timer % 60 == 0) min -=1;
	timer--;	
}

function start_game() {
	init();
	timer = 145;
        min = Math.round(timer / 60);
        sec = Math.round(timer % 60);
	setInterval(update_timer,1000);
}

function update_timer(t) {
	dec_timer();
	sec = Math.round(timer % 60);
	
	if(sec < 10) sec = "0" +sec;

	document.getElementById("timer").innerHTML =  min +":" + sec ;
}

function spawn_calc(sign) {
	difficulty = calc[3];
	if(sign == 'x') calc[3] = 10;
  else if(sign == '-') calc[3] = 900;
  else if(sign == '+') calc[3] = 900; 

	calc[0] = Math.floor(Math.random() * calc[3]);
	calc[1] = Math.floor(Math.random() * calc[3]);
	
  if(sign =="x") {
    console.log("[SPAWN_CALC L163]: difficulty: " , calc[3]);
	}
  if(sign == '-' && (calc[0] - calc[1] <= -1)) spawn_calc("-")
	calc[2] = convert_calc(calc[0],calc[1],sign);
//	calc[3] = difficulty;	
	return calc;
}

function convert_calc(val1,val2,sign) {
	if(sign == '+')
		return val1 + val2;
	else if(sign == '-') { 
		if((val1 - val2) < 0) 
			show_calc();
		return val1 - val2;
	}
	else if (sign == 'x')
		return val1 * val2;
	else if(sign =='/')
		return val1 / val2;
}

var switcher = 0;
function sel_sign() {
        if(add == 1)
            add = 1;
        //add+mul
        if(add == 1 && mul == 1)
            console.log("Add + Mul selected");
            sign = Math.floor(Math.random() * 3); 
            if (sign == 1)
                sign = switcher;
                switcher ? 0 : 1;
        if(sub == 1)
            sign = 1;
        if(add == 1 && sub ==1)
            sign = Math.floor(Math.random() * 2);
        //sub + mul
        if(mul == 1) 
            sign = 2;
        if (add ==1 && sub ==1 && mul==1) 
	    sign = Math.floor(Math.random() * 3); 
        
        console.log("SWITCHER:",switcher);
	switch(sign) {
		case 0:
			return '+'; break;
		case 1:
			return '-'; break;
		case 2:
			return 'x'; break;
		case 3: 
			return '/'; break;
	}
}

function show_calc() {
	var sign = sel_sign()
	var val = spawn_calc(sign);
	var calc = document.getElementById('calc');
	calc.innerHTML= val[0] + "  " +  sign + "   " + val[1] + " =     ";
}

function check_result(response) {
	if(response == calc[2]) {
		clear_num();
		update_score(5);
		update("good");
		show_calc();
	} else {
		var tmp_calc = document.getElementById('calc').innerHTML + response;
	 	console.log("Calc:  Response:",tmp_calc);
		errors.push(tmp_calc);
		update("wrong");
		clear_num(); }
}

function clear_num() {
	res_input.innerHTML = "";
}

function delete_num() {
	res_input.innerHTML = "";
}

function print_num(num) {
	if(res_input.innerHTML.length < calc[2].toString().length) {
		if(res_input.innerHTML === '__') {
			res_input.innerHTML = num;
		} else {
			res_input.innerHTML += num;
		}
	}
	
	if(res_input.innerHTML.length === calc[2].toString().length) {
		setTimeout(check_result,500, res_input.innerHTML);
	}
}	

