var url = 'https://cbtech.codeberg.page/PMT/Themes/';
var theme;
var bg_color,fg_color,cursor_bg,cursor_fg;
color = [];


function loadFile(url) {
   fetch(url).then((response) => response.text())
   .then((colorscheme) => {
    document.getElementById("colorscheme").innerHTML = colorscheme;
   });
}

function read_file(theme) {
  full_url = url + theme;
  loadFile(full_url);
}

/* TMUX DESIGNER */
