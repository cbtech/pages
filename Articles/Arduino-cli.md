Maitriser l'arduino en ligne de commande grâce à l'outil arduino-cli
https://github.com/avrdudes/avr-libc
Pour obtenir la liste des carte connectées via la ligne de commande 
    arduino-cli board list

Vous obtiendrez une sortie similaire à celle-ci

Port         Type              Board Name       FQBN                Core
/dev/ttyACM0 Serial Port (USB) Arduino Uno      arduino:avr:uno     arduino:avr

Pour installer les cores nécessaire pour les différentes carte arduino
    
        arduino-cli core update-index
    
    Vous pouvez effectuer une recherche grâce à la command
        arduino-cli core search
    
    et puis installer le core choisit avec la commande
    Pour mon cas, possèdant une carte mega2560
        arduino-cli core install arduino:avr

Qu'est ce qu'un core:

    Abstraction layer:
        Le core fournit une abstraction materiel pour évite
        d'avoir à manipuler le code complex du matériel.
        Cette couche permet aux dévelloppeur d'interagir avec
        le matériel à un niveau plus élevé, sans se soucier des spécificités
        matérielles.

        Voici un exemple sans l'abtraction du core
  
                #include <avr/io.h>

                    int main(void) {
                    // Configure the pin as an output
                    DDRB |= (1 << PB5);

                    while (1) {
                    // Toggle the LED
                    PORTB ^= (1 << PB5);
                    // Delay
                    _delay_ms(500);
                    }

                     return 0;
               }

    Hardware support

        Arduino AVR Core:
            supporte les boards Arduino Uno,Mega et Nano

        Arduino SAMD Core:
            Supporte les boards Arduino Zero et les series MKR
    
        ESP8266
            supporte les microcontrolleurs ESP8266 et ESP32,
            utilisé pour le Wifi et le Bluetooth.
    
         MegaAVR
            Support pour les microprocesseurs  ATMega4809 fournit
            dans les arduinos Uno Wifi.

    Librairie integration
            le core arduino inclut  les librairies nécessaire pour la
            communication serie, les entrée/sorties analogue et digital
            les fonctionnalitées pour le timming et pleins d'autre.

Lister les cores
        arduino-cli core list

Creation du sketch :
        Le sketch est un code source écrit pour être éxecuté sur l'arduino
        Il possède l'extension .ino.
        Un sketch peut inclure des fichiers .c et .h pour améliorer votre 
        code.
                arduino-cli sketch new BlinkMe

Compile
        -b fournit le type de board, pour cela  faite la commande 
         arduino-cli board list et regarder la valuer de FQBN(Fully Qualified Board name)
         Le FQBN  est utilisé par l'outil pour distinguer les différents type de carte.

                arduino-cli compile -b arduino:avr:mega -v --output-dir .

Téléversement:
        Le téléversement permet d'uploader votre code sur votre carte arduino 
        via votre connection USB.Vous apercevrez les LED TX/RX clignotées ce qui 
        veut dire que votre carte se met à jour.
            
            arduino-cli upload -b arduino:avr:mega -p /dev/ttyACM0 -v

Gràce à ce tutoriel, vous n'aurez pas besoin d'installer d'IDE et vous 
permettra de travailller en ligne de commande avec votre éditeur favorit. 
