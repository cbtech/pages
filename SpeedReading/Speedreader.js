var url = 'https://cbtech.codeberg.page/SpeedReading/Stories/';
var story = "story01";
var full_url = url + story + '.txt';
var words,wpm;
var startTime;
var min = sec = msec = 0;
var start = false;
var timer; 

function update_timer() {
  sec++;

  if(sec == 60) {
    sec = 0;
    min++;
  }
  sec  = ("0" + sec).slice(-2);
  min  = ("0" + min).slice(-2);

  document.getElementById("time").innerHTML = min + ":" + sec ; 

  updateWPM();
  timer = setTimeout(update_timer,1000);
}

function updateWPM() {
  wpm = (new Date).getTime() - startTime;
  reading_speed = (words / wpm) * 60; 
  console.log("updateWPM: ", reading_speed.toFixed(0));
  document.getElementById("reading_speed").innerHTML = reading_speed.toFixed(0);
}

function loadFile(url) {
  fetch(url).then((response) => response.text())
   .then((new_text) => {
    document.getElementById("text").innerHTML= new_text;
    words = new_text.split(" ").length;
     console.log("Words in text: ", words);
   });
}

function start_BTN() {
  startTime = (new Date).getTime();
  update_timer();
  document.getElementById("start_btn").disabled = true;
  document.getElementById("start_btn").style.backgroundColor="gray";
}

function stop_BTN() {
  document.getElementById("start_btn").disabled = false;
  document.getElementById("start_btn").style.backgroundColor="dodgerblue";
  clearTimeout(timer);
}

function update_story() {
  story = document.getElementById("stories").value;
  full_url = url + story + '.txt'
  console.log("Story: ", full_url);
   loadFile(full_url);
}

loadFile(full_url);
